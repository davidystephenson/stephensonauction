var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Auction' });
});

router.get('/auction/:name', function(req, res) {
  res.render('auction', { title: 'Auction: ' + req.params.name, name: req.params.name });
});

module.exports = router;
