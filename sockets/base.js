var Auction = require('../lib/auction.js');
var Tools = require('../lib/tools.js');

module.exports = function(io) {
  var auctions = {};

  var tools = Tools();

  io.on('connection', function(socket) {
    var leave = function() {
      if (socket.bidder) {
        auctions[socket.auction].leave(socket.bidder);
        socket.bidder = false;
      }
    };

    socket.bidder = false;
    socket.auction = false;

    socket.on('disconnect', function() { leave(); });

    // Index
    socket.on('create', function(name) {
      notification = tools.invalidate(name, 'auction', auctions);
      if (notification) socket.emit('notification', notification);
      else {
        auctions[name] = new Auction(io, name);
        socket.emit('created', name);
        socket.broadcast.emit('auction', name);
      }
    });

    socket.on('index', function() {
      for (var auction in auctions) { socket.emit('auction', auction); }
    });

    // Auction
    socket.on('join', function(bidder) {
      if (socket.bidder) {
        socket.emit('notification', 'You have already joined as ' + socket.bidder);
      }
      else {
        notification = tools.invalidate(bidder, 'bidder', auctions[socket.auction].bidders);

        if (notification) socket.emit('notification', notification);
        else {
          if (auctions[socket.auction].size == auctions[socket.auction].maximum) {
            socket.emit(
              'notification', 'This auction has the maximum number of bidders.'
            );
          } else {
            socket.bidder = bidder;
            auctions[socket.auction].join(bidder, socket, socket.id);
          }
        }
      }
    });

    socket.on('leave', function () { leave(); });

    socket.on('raise', function(value) {
      if (/^\+?[1-9]\d*$/.test(value)) {
        value = parseInt(value);
        if (value > auctions[socket.auction].bidders[socket.bidder].bid) {
          auctions[socket.auction].tally();
          if (value >= auctions[socket.auction].highest) {
            if (auctions[socket.auction].time <= 5) {
              auctions[socket.auction].time = 5; 
          }
            auctions[socket.auction].raise(socket.bidder, value);
          } else {
            socket.emit('notification', 'You must bid greater than or equal to the highest bid.');
          }
        } else {
          socket.emit('notification', 'You must bid higher than your current bid.');
        }
      } else {
        socket.emit('notification', 'Bids must be integers greater than 0');
      }
    });

    socket.on('choose', function(pick) { auctions[socket.auction].choose(pick); });

    socket.on('start', function() { auctions[socket.auction].start(); });

    socket.on('withdraw', function() { auctions[socket.auction].withdraw(socket.bidder); });

    socket.on('visited', function(name) {
      if (!(name in auctions)) {
        notification = tools.invalidate(name, 'auction', auctions);
        if (notification) {
          socket.emit('notification', notification);
          return false;
        }
        auctions[name] = new Auction(io, name);
        io.emit('auction', name);
      }

      if (auctions[name].full) {
        socket.emit('full');
        socket.emit(
          'notification',
          'This auction already has the maximum number of bidders. ' +
          'You can still spectate until someone leaves.'
        );
      } else {
        socket.emit('empty');
        socket.emit('notification', 'Press Join to participate in the bidding.');
      }

      socket.join(name);
      socket.auction = name;

      for (var bidder in auctions[socket.auction].bidders) {
        socket.emit('bidder', auctions[socket.auction].bidders[bidder]);
      }
    });
  });
};
