var Tools = require('./tools.js');

module.exports = function(auction) {
    var auctioneer = {};

    auctioneer.auction = auction;

    auctioneer.announce = function(winner, lot, call, highest) {
        auctioneer.post(
            auctioneer.tools.buff(winner) + ' won ' + auctioneer.tools.buff(lot) +
            ' with a ' + call + ' ' + auctioneer.tools.buff(highest) + '.'
        );
    };

    auctioneer.broadcast = function(bidder, type, content) {
        bidder.socket.to(auction.name).broadcast.emit(type, content);
    };

    auctioneer.inform = function(bidder, notification) {
        auctioneer.whisper(bidder, 'notification', notification);
    };

    auctioneer.post = function(notification) { auctioneer.yell('notification', notification); };

    auctioneer.tools  = Tools();

    auctioneer.whisper = function(bidder, type, content) {
        try { auction.io.sockets.connected[bidder.socketID].emit(type, content); }
        catch(TypeError) {
            console.log('Warning: Whisper failed due to TypeError.');
            auctioneer.auction.cancel();
        }
    };

    auctioneer.yell = function(type, content) { auctioneer.auction.io.to(auction.name).emit(type, content); };

    return auctioneer;
}; 
