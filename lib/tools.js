module.exports = function() {
    var tools = {};

    tools.buff = function(text) { return '<strong>' + text + '</strong>'; };

    tools.invalidate = function(text, type, set) {
        var notification = false;
        if (text) {
            if (/^[A-Za-z0-9]+$/.test(text)) {
                if (text in set) {
                    notification = tools.buff(text) + ' is currently being used by another ' + type + '.';
                    return false;
                } else {
                    return false;
                }
            } else {
                notification = 'That ' + type + ' contains illegal characters or spaces.';
            }
        } else {
            notification = 'That ' + type + ' is empty.';
        }

        return notification;
    };
    tools.print = function(obj) { return JSON.stringify(obj, null, 4); };

    tools.randomElement = function(array) {
        return array[Math.floor(Math.random() * array.length)];
    };

    tools.randomKey = function(set) {
        return Object.keys(set)[Math.floor(Math.random() * Object.keys(set).length)];
    };

    return tools;
};
