module.exports = function() {
    var bidder = {};

    bidder.bid = 0;
    bidder.bidding = false;
    bidder.lots = [];
    bidder.missing = 5;
    bidder.name = '';
    bidder.pool = 100;
    bidder.socket = 0;
    bidder.socketID = 0;

    return bidder;
};
