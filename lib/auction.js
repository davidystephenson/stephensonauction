var _ = require('underscore');
var Auctioneer = require('./auctioneer.js');
var Bidder = require('./bidder.js');
var Choice = require('./choice.js');
var heroes = require('./heroes.json');
var Tools = require('./tools.js');

module.exports = function(io, name) {
  var auction = {};

  auction.auctioneer = Auctioneer(auction);

  auction.ban = function() {
    auction.auctioneer.post(
      auction.tools.buff(auction.chooser.name) + ' bans ' + auction.tools.buff(auction.current) + '.'
    );
    auction.auctioneer.yell('ban', auction.current);
    auction.transfer();
  };

  auction.bidders = {};

  auction.bidding = function() {
    auction.openBidding(auction.chosen);
    auction.current = auction.chosen;
    auction.raise(auction.chooser.name, 1);
    auction.clock(10, auction.call);
  };

  auction.call = function() {  
    auction.closeBidding(); 

    if (auction.winner) {
      auction.auctioneer.announce(
        auction.winner,
        auction.current,
        'bid of',
        auction.highest
      );
      auction.bidders[auction.winner].pool -= auction.highest;
      auction.collect(); // Reset bids with correct pool.
      if (auction.bidders[auction.winner].missing) {
        auction.choice = Choice(
          auction, auction.resolution, auction.resolutions, 'Resolution', 10
        );
      }
      else auction.ban();
    }
    else {
      auction.auctioneer.announce('No one', auction.current, 'tie at', auction.highest);
      auction.collect(); // Reset bids with correct pool.
      auction.ban();
    }
  };

  auction.cancel = function() {
    auction.auctioneer.post('The auction has been cancelled because too many bidders have left.');
    auction.auctioneer.yell('cancelled');
    auction.end();
  };

  auction.choose = function(choice) {
    if (_.contains(auction.choice.options, choice)) {
      auction.killIntervals();
      if (auction.choosing) {
        auction.choosing = false;
        auction.auctioneer.yell('chosen'); // Hide the choice panels.
        auction.auctioneer.yell('tick', 0);

        auction.chosen = choice; 

        auction.auctioneer.post(
          auction.tools.buff(auction.chooser.name) + ' chooses ' + auction.tools.buff(choice) + '.'
        );
        auction.timeOff(); // Make the timer inactive.
        auction.choice.next();
      }     
    } else {
      auction.auctioneer.inform(auction.chooser, 'Please select from the list of choices.');
    }
  };

  auction.chooser = 'Warning: No-one is the choser.';

  auction.choosing = false;

  auction.chooseRandom = function() {
    auction.choose(auction.tools.randomElement(auction.choice.options));
  };

  auction.clock = function(seconds, alarm) {
    auction.time = seconds;
    auction.alarm = alarm;

    auction.auctioneer.yell('time on'); // Make the timer active.
    auction.intervals.push(setInterval(function() { auction.tick(); }, 1000) );
  };

  auction.closeBidding = function() {
    auction.auctioneer.yell('gone'); // Hide the raise panel.
    auction.auctioneer.yell('untied'); // Hide the withdrawal panel.
    auction.tally();
  };

  auction.collect = function() {
    for (var bidder in auction.bidders) {
      auction.bidders[bidder].bid = 0;
      auction.auctioneer.yell('bid', auction.bidders[bidder]);
    }
  };

  auction.count = function() {
    auction.size = _.size(auction.bidders);
    auction.ready = false;
    auction.full = false;

    var notification = '';

    if (auction.size < auction.minimum) {
      auction.going = false;
      auction.auctioneer.yell('waiting'); // Hide the raise and start buttons.
      var remaining = auction.minimum - auction.size;
      notification = 'Bidding can begin when ' + remaining + ' more bidders have joined.';
    } else {
      if (!auction.going) {
        auction.auctioneer.yell('ready');
        auction.ready = true;
        notification = 'Press Start to begin bidding.';
      }
    }

    if (auction.size == auction.maximum) {
      auction.auctioneer.yell('full');
      auction.full = true;
      notification = 'This auction has the maximum number of bidders. Press Start to begin bidding.';
    }
    
    if (!auction.going) {
      auction.auctioneer.post(notification);
    }
  };

  auction.end = function() {
    auction.killIntervals();
    auction.going = false;
    auction.count();

    for (var bidder in auction.bidders) {
      auction.bidders[bidder].lots = [];
      auction.bidders[bidder].missing = 5;
      auction.bidders[bidder].pool = 100;
    }

    if (auction.size > 1) {
      auction.auctioneer.yell('clear'); // Hide choice panels and reset the block.
      auction.auctioneer.yell('reset'); // Show bids as 0 / 100.
      auction.heroes = heroes;
    }
  };

  auction.going = false;

  auction.heroes = heroes;

  auction.highest = 0;

  auction.io = io;

  auction.intervals = [];

  auction.join = function(name, socket, socketID) {
    auction.bidders[name] = new Bidder();
    auction.bidders[name].socketID = socketID;
    auction.bidders[name].name = name;

    auction.auctioneer.whisper(auction.bidders[name], 'joined', auction.bidders[name]);
    auction.auctioneer.yell('bidder', auction.bidders[name]);
    auction.auctioneer.post(auction.tools.buff(name) + ' has joined.');
    auction.count();
  };

  auction.killIntervals = function() { auction.intervals.forEach(clearInterval); };

  auction.leave = function(bidder) {
    auction.auctioneer.post(auction.tools.buff(bidder) + ' has left.');
    auction.auctioneer.whisper(auction.bidders[bidder], 'spectator');
    auction.auctioneer.yell('left', bidder);

    auction.size --;

    if (auction.size < auction.minimum && auction.going) {
      auction.auctioneer.whisper(auction.bidders[bidder], 'valid');
      delete auction.bidders[bidder];
      auction.auctioneer.cancel();
    } else  {
      delete auction.bidders[bidder];
      auction.count();
    }
  };

  auction.maximum = 2;

  auction.minimum = 2;

  auction.name = name;

  auction.nextLot = function() {
    auction.chooser = auction.bidders[auction.chosen];
    auction.choice = Choice(auction, auction.bidding, auction.heroes, 'Hero', 20);
  };

  auction.nextChooser = function() {
    auction.choice = Choice(
      auction, auction.nextLot, Object.keys(auction.bidders), 'Bidder', 10
    );
  };

  auction.openBidding = function(block) {
    for (var bidder in auction.bidders) { auction.bidders[bidder].bidding = true; }
    auction.auctioneer.yell('open'); // Show the raise panel.
    auction.auctioneer.current = block;
    auction.auctioneer.yell('block', auction.tools.buff(block));
    auction.auctioneer.yell('expected', 1);
  };

  auction.pick = function() {
    auction.auctioneer.post(
      auction.tools.buff(auction.chooser.name) + ' picks ' + auction.tools.buff(auction.current) + '.'
    );
    
    auction.bidders[auction.winner].lots.push(auction.current);
    auction.auctioneer.yell('return', auction.winner);
    auction.auctioneer.yell('won', auction.bidders[auction.winner]);
    auction.bidders[auction.winner].missing --;
    
    auction.transfer();
  };

  auction.raise = function(name, value) {
    if (name in auction.bidders) {
      var bidder = auction.bidders[name];
      if (value <= bidder.pool) {
        if (bidder.pool - value >= bidder.missing) {
          if (bidder.bidding) {
            bidder.bid = value;
            auction.auctioneer.yell('bid', bidder);
            auction.auctioneer.post(
              auction.tools.buff(name) + ' bids ' + auction.tools.buff(value) + '.'
            );
            auction.tally();
          } else {
            auction.auctioneer.inform(
              auction.bidders[name],
              'You cannot bid after withdrawing on an item.'
            );
          }
        } else {
          auction.auctioneer.inform(
            auction.bidders[name],
            'You must have at least one in your pool for every lot you need.'
          );
        }
      } else {
        auction.auctioneer.inform(auction.bidders[name], 'You cannot bid more than your pool.');
      }
    } else { console.log('Warning: Non-bidder tried to bid.'); }
  };

  auction.ready = false;

  auction.required = 5;

  auction.resolution = function () {
    if (auction.chosen == 'Pick') auction.pick();
    else auction.ban();
  };

  auction.resolutions = ['Ban', 'Pick'];

  auction.setup = function() {
    auction.closeBidding();
    if (auction.winner) {
      auction.auctioneer.announce(auction.winner, auction.current, 'bid of', auction.highest);
      auction.bidders[auction.winner].pool -= auction.highest;
      auction.collect(); // Reset bids with correct pool.
      auction.choice = Choice(auction, auction.side, auction.sides, 'Side', 10);
    } else {
      auction.collect();
      auction.auctioneer.announce('No one', auction.current, 'tie at', auction.highest);

      var radiant = auction.tools.randomKey(auction.bidders);
      auction.bidders[radiant].lots.push('Radiant');
      auction.auctioneer.yell('won', auction.bidders[radiant]);

      for (var bidder in auction.bidders) {
        if (auction.bidders[bidder].name != radiant) {
          auction.bidders[bidder].lots.push('Dire');
          auction.auctioneer.yell('won', auction.bidders[bidder]);
        }
      }

      auction.chooser = auction.bidders[auction.tools.randomKey(auction.bidders)];
      auction.auctioneer.yell('untied');
      auction.nextChooser(); 
    }
  };

  auction.side = function() {
    auction.bidders[auction.winner].lots.push(auction.chosen);
    auction.auctioneer.yell('won', auction.bidders[auction.winner]);

    var other = '';
    if (auction.chosen == 'Radiant') other = 'Dire';
    else other = 'Radiant';

    for (var bidder in auction.bidders) {
      if (auction.bidders[bidder].name != auction.winner) {
        auction.bidders[bidder].lots.push(other);
        auction.auctioneer.yell('won', auction.bidders[bidder]);
      }
    }
    auction.nextChooser();
  };

  auction.sides = ['Radiant', 'Dire'];

  auction.size = 0;

  auction.start = function() {
    auction.auctioneer.yell('reset'); // Show bidders as 0 / 100.
    for (var bidder in auction.bidders) {
      auction.auctioneer.yell('return', bidder); // Show bidders with no lots.
    }

    auction.going = true;
    auction.auctioneer.post('The auction is beginning.');

    auction.openBidding('Pick and Side Choice');
    auction.clock(10, auction.setup);
  };

  auction.tally = function() {
    auction.highest = 0;
    auction.winner = false;

    for (var bidder in auction.bidders) {
      if (auction.bidders[bidder].bid > auction.highest) {
        auction.winner = bidder;
        auction.highest = auction.bidders[bidder].bid;
      } else if (auction.bidders[bidder].bid == auction.highest) auction.winner = false;
    }
    
    if (auction.winner) {
      auction.auctioneer.yell('expected', auction.highest);
      auction.auctioneer.yell('untied');
      auction.chooser = auction.bidders[auction.winner];
      auction.auctioneer.whisper(auction.bidders[auction.winner], 'winning');
      for (bidder in auction.bidders) {
        if (bidder != auction.winner) {
          auction.auctioneer.whisper(auction.bidders[bidder], 'losing');
        }
      }
    } else {
      auction.auctioneer.yell('expected', auction.highest + 1);
      for (bidder in auction.bidders) {
        if (auction.bidders[bidder].bid == auction.highest) {
          auction.auctioneer.whisper(auction.bidders[bidder], 'tied');
        } else {
          auction.auctioneer.whisper(auction.bidders[bidder], 'losing');
        }
      }
    }
  };

  auction.tick = function(fn) {
    auction.auctioneer.yell('tick', auction.time);
    auction.time --;

    if (auction.time < 0) {
      auction.timeOff();
      auction.killIntervals();
      auction.alarm();
    }
  };

  auction.timeOff = function() { auction.auctioneer.yell('time off'); };

  auction.transfer = function() {
    var lot = auction.heroes[auction.current];

    auction.heroes = _.without(auction.heroes, auction.current);
    
    if (auction.going) {
      var missing = false;
      for (var bidder in auction.bidders) {
        if (auction.bidders[bidder].missing > 0) missing = bidder;
      }

      if (missing) auction.nextChooser();
      else {
        auction.auctioneer.post('The auction is over.');
        auction.end(); 
      }
    }
  };
   
  auction.time = 0;

  auction.winner = false;

  auction.withdraw = function(name) {
    if (name in auction.bidders) {
      var bidder = auction.bidders[name];
      bidder.bid = 0; 
      bidder.bidding = false; 
      auction.auctioneer.yell('bid', bidder);
      auction.tally();
      var bidding = 0;
      for (var player in auction.bidders) {
        if (auction.bidders[player].bidding) bidding ++;
      }
      if (bidding < 2) {
        auction.killIntervals();
        auction.alarm();
      }
    }
  };

  auction.tools = new Tools();

  return auction;
}; 
