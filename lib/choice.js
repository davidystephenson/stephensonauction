var Tools = require('./tools.js');

module.exports = function(auction, next, options, name, seconds) {
    var choice = {};

    choice.next = next;
    choice.options = options;
    choice.name = name;
    choice.tools = Tools();
    
    auction.auctioneer.yell('choosing'); // Hide the Start button.
    auction.auctioneer.yell('clear'); // Remove all choices.
    
    auction.auctioneer.yell('choice', choice);

    var message = choice.tools.buff(auction.chooser.name) + ' is choosing a ' + choice.name + '.';
    auction.auctioneer.yell('block', message);
    auction.auctioneer.post(message);

    // Show the choice panel.
    auction.auctioneer.whisper(auction.chooser, 'chooser');
    auction.auctioneer.inform(auction.chooser, 'Please choose a ' + choice.name + '.');

    auction.time = 10;
    auction.choosing = true;
    auction.killIntervals();
    auction.clock(seconds, auction.chooseRandom);

    return choice;
};
