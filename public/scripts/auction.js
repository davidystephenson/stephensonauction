var joined = false;
var name = $('#name').text();
var socket = io();

var notify = function(notification) {
  var notifications = $('#notifications').children();
  if (notifications.length > 9) notifications.last().remove();

  notifications.filter('.active').removeClass('active');
  
  $('#notifications').prepend('<a class="list-group-item active">' + notification + '</a>');
};

var print = function(obj) { return JSON.stringify(obj, null, 4); };

$('#sign-on').hide();
$('#sign-off').hide();
$('#raise').hide();
$('#begin').hide();
$('#choice').hide();
$('#banned').hide();
$('#withdrawal').hide();

socket.emit('visited', name);

//Input
socket.on('ban', function(lot) {
  $('#bans').append('<a class="list-group-item">' + lot + '</a>');
  $('#banned').show();
  $('#withdrawal').hide();
});

socket.on('bid', function(bidder) {
  $('#' + bidder.name + '-bid').text(bidder.bid);
  $('#' + bidder.name + '-pool').text(bidder.pool);
});

socket.on('bidder', function(bidder) {
  var active = '';
  if (bidder.name == joined) active = ' active';
  $('#bidders').append(
    '<a id="' + bidder.name + '" class="list-group-item' + active + '">' + 
      bidder.name +
      '<span class="badge">' +
        '<span id="' + bidder.name + '-bid" class="bid">' + bidder.bid + '</span> / ' +
        '<span id="' + bidder.name + '-pool" class="pool">' + bidder.pool + '</span>' +
      '</span>' + 
      '<div id="' + bidder.name + '-lots"></div>' +
    '</a>'
  );
});

socket.on('block', function(lot) { $('#current').html(lot); });

socket.on('cancelled', function() {
  $('#choice').hide();
  $('#raise').hide();
  $('#withdrawal').hide();
});

socket.on('choice', function(choice) {
  $('#type').text(choice.name);

  choice.options.forEach(function(option) {
    $('#choices').append('<a class="list-group-item choice">' + option + '</a>');
  });

  $('#complete').val('');
  $('#complete').autocomplete({ source: choice.options });
});

socket.on('chosen', function(type) { $('#choice').hide(); });

socket.on('choosing', function() { $('#begin').hide(); });

socket.on('chooser', function() {
  $('#choice').show();
  $('#complete').focus();
});

socket.on('clear', function() {
  $('#choice').hide();
  $('#choices').empty();
  $('#current').html('-');
});

socket.on('disconnect', function() {
  $('#sign-on').hide();
  $('#sign-off').hide();
  $('#raise').hide();
  $('#choice').hide();
  notify('This auction has been canceled. Please reload the page.');
});

socket.on('empty', function() { $('#sign-on').show(); });

socket.on('expected', function(value) { $('#value').val(value); });

socket.on('full', function() { $('#sign-on').hide(); });

socket.on('gone', function() {
  $('#raise').hide();
  $('#withdrawal').hide();
});

socket.on('joined', function(bidder) {
  joined = bidder.name;
  $('#sign-on').hide();
  $('#sign-off').show();
});

socket.on('left', function(bidder) { $('#' + bidder).remove(); });

socket.on('losing', function() {
  $('#timer').switchClass(
    'list-group-item-info list-group-item-success', 'list-group-item-danger'
  );
  $('#timer-value').switchClass('info success', 'danger');
});

socket.on('open', function() { $('#raise').show(); });

socket.on('notification', function(notification) { notify(notification); });

socket.on('ready', function() { if (joined) { $('#begin').show(); } });

socket.on('reset', function() {
  $('.bid').text('0');
  $('.pool').text('100');
});

socket.on('return', function(name) { $('#' + name + '-lots').empty(); });

socket.on('spectator', function(bidder) {
  joined = false;
  $('#sign-off').hide();
  $('#raise').hide();
  $('#begin').hide();
  $('#sign-on').show();
});

socket.on('tick', function(time) { $('#timer-value').text(time); });

socket.on('tied', function() {
  $('#withdrawal').show();
  $('#timer').switchClass(
    'list-group-item-success list-group-item-danger', 'list-group-item-info'
  );
  $('#timer-value').switchClass('danger success', 'info');
});

socket.on('time off', function() { $('#timer').removeClass('active'); });

socket.on('time on', function() { $('#timer').addClass('active'); });

socket.on('untied', function() { $('#withdrawal').hide(); });

socket.on('waiting', function() {
  $('#raise').hide();
  $('#begin').hide();
});

socket.on('winning', function() {
  $('#timer').switchClass(
    'list-group-item-info list-group-item-danger', 'list-group-item-success'
  );
  $('#timer-value').switchClass('info danger', 'success');
});

socket.on('won', function(bidder) {
  bidder.lots.forEach(function(lot) {
    $('#' + bidder.name + '-lots').append(
      '<div class="bidder-lot">' + lot + '</div>'
    );
  });
  $('#withdrawal').hide();
});

// Output
var raise = function() { socket.emit('raise', $('#value').val()); };

var withdraw = function() { socket.emit('withdraw'); };

$('#bid').click(function() { raise(); });

$('#choose').click(function() { socket.emit('choose', $('#complete').val()); });

$('#join').click(function() { socket.emit('join', $('#bidder').val()); });

$('#leave').click(function() {
  socket.emit('leave');
  $('#sign-on').show();
  $('#sign-off').hide();
  $('#begin').hide();
});

$('#start').click(function() {
  $('#banned').hide();
  socket.emit('start');
});

$('#choices').on('click', '.choice', function() { socket.emit('choose', $(this).text()); });

$('#withdraw').click(function() { withdraw(); });

// Hotkeys
$(document).bind('keydown', 'space', function() { console.log('space'); raise(); });

$(document).bind('keydown', 'Shift+space', function() { console.log('shift space'); withdraw(); });
